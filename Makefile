CC = cc
OBJS = pgpcrack.o idea.o md5.o #memmove.o
# Uncomment "memmove.o" if you are using SunOS or if the platform you are using
# does not support memmove()

LDFLAGS = -s
CFLAGS = -O2 -DIDEA32 #-DHIGHFIRST
# Uncomment "-DHIGHFIRST" if you are compiling this for a big-endian machine
# Delete -DIDEA32 if you find that this is faster without it

pgpcrack: $(OBJS)
	$(CC) $(LDFLAGS) -o pgpcrack $(OBJS)

pgpcrack.o: pgpcrack.c
	$(CC) $(CFLAGS) -c pgpcrack.c

idea.o: idea.c
	$(CC) $(CFLAGS) -c idea.c

md5.o: md5.c
	$(CC) $(CFLAGS) -c md5.c	

memmove.o: memmove.c
	$(CC) $(CFLAGS) -c memmove.c

clean:
	rm -f *.o *~ pgpcrack core
